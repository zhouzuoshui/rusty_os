# 基础环境

这是一个基于 Rust 的操作系统，我有一个 Jetson nano 开发板，以此为开发机。不过我并不想把它搞崩，为了方便，我们不关心硬件，这个项目运行在 qemu 上。在 Ubuntu 18.04 上可以这样配置环境：

```sh
sudo apt install gdb-multiarch qemu-system-misc qemu-system-arm qemu-efi-aarch64 qemu-utils
# rust
rustup target add aarch64-unknown-none
cargo intall cargo-binutils
rustup component add llvm-tools-preview
rustup component add rust-src
```

## 启动

我们是不关心 bootloader 的，启动这个内核的时候可以直接指定内核：
```sh
qemu-system-aarch64 -machine virt -cpu cortex-a57 -smp 1 -m 2G -kernel target/aarch64-unknown-none/release/kernel.bin -nographic
```

这时，系统会从 0x40000000 启动，执行一段之后就会在 0x40080000 处把操作权交给我们的内核，因此我们的操作系统的入口点就设在此处。

## 调试

如果要用 gdb 调试，则可以这样

```sh
qemu-system-aarch64 -machine virt -cpu cortex-a57 -smp 1 -m 2G -kernel target/aarch64-unknown-none/release/kernel.bin -nographic -s -S
```

然后另起一个终端

```sh
gdb -ex 'file target/aarch64-unknown-none/release/hello-world' \
    -ex 'set arch aarch64' \
    -ex 'target remote localhost:1234'
```

## uboot 作 BIOS

直接用 qemu 启动并没有使用 BIOS，这与实际情况是不一致的。uboot 是常用的 arm64 开发板的 BIOS 系统。虽然 bootloader 也不是我们关心的，但是如果需要的话，可以直接使用 uboot ，其在 Ubuntu 仓库里有 qemu 的版本。启动 bootloader：

```sh
sudo apt install u-boot-qemu
qemu-system-aarch64 -curses -machine virt -cpu cortex-a57 -bios /usr/lib/u-boot/qemu_arm64/u-boot.bin -serial stdio
```

进入 uboot 的命令行之后，输入 `printenv` ，得到如下输出

```
arch=arm
baudrate=115200
board=qemu-arm
board_name=qemu-arm
boot_a_script=load ${devtype} ${devnum}:${distro_bootpart} ${scriptaddr} ${prefix}${script}; source ${scriptaddr}
boot_efi_binary=if fdt addr ${fdt_addr_r}; then bootefi bootmgr ${fdt_addr_r};else bootefi bootmgr ${fdtcontroladdr};fi;load ${devtype} ${devnum}:${distro_bootpart} ${kernel_addr_r} efi/boot/bootaa64.efi; if fdt addr ${fdt_addr_r}; then bootefi ${kernel_addr_r} ${fdt_addr_r};else bootefi ${kernel_addr_r} ${fdtcontroladdr};fi
boot_extlinux=sysboot ${devtype} ${devnum}:${distro_bootpart} any ${scriptaddr} ${prefix}${boot_syslinux_conf}
boot_net_usb_start=usb start
boot_pci_enum=pci enum
boot_prefixes=/ /boot/
boot_script_dhcp=boot.scr.uimg
boot_scripts=boot.scr.uimg boot.scr
boot_syslinux_conf=extlinux/extlinux.conf
boot_targets=usb0 scsi0 virtio0 dhcp
bootcmd=run distro_bootcmd
bootcmd_dhcp=run boot_net_usb_start; run boot_pci_enum; if dhcp ${scriptaddr} ${boot_script_dhcp}; then source ${scriptaddr}; fi;setenv efi_fdtfile ${fdtfile}; setenv efi_old_vci ${bootp_vci};setenv efi_old_arch ${bootp_arch};setenv bootp_vci PXEClient:Arch:00011:UNDI:003000;setenv bootp_arch 0xb;if dhcp ${kernel_addr_r}; then tftpboot ${fdt_addr_r} dtb/${efi_fdtfile};if fdt addr ${fdt_addr_r}; then bootefi ${kernel_addr_r} ${fdt_addr_r}; else bootefi ${kernel_addr_r} ${fdtcontroladdr};fi;fi;setenv bootp_vci ${efi_old_vci};setenv bootp_arch ${efi_old_arch};setenv efi_fdtfile;setenv efi_old_arch;setenv efi_old_vci;
bootcmd_scsi0=devnum=0; run scsi_boot
bootcmd_usb0=devnum=0; run usb_boot
bootcmd_virtio0=devnum=0; run virtio_boot
bootdelay=2
bootfile=boot.scr.uimg
cpu=armv8
distro_bootcmd=scsi_need_init=; setenv nvme_need_init; virtio_need_init=; for target in ${boot_targets}; do run bootcmd_${target}; done
efi_dtb_prefixes=/ /dtb/ /dtb/current/
ethact=virtio-net#32
ethaddr=52:52:52:52:52:52
fdt_addr=0x40000000
fdt_high=0xffffffff
fdtcontroladdr=46ee2de0
initrd_high=0xffffffff
kernel_addr_r=0x40400000
load_efi_dtb=load ${devtype} ${devnum}:${distro_bootpart} ${fdt_addr_r} ${prefix}${efi_fdtfile}
nvme_boot=run boot_pci_enum; run nvme_init; if nvme dev ${devnum}; then devtype=nvme; run scan_dev_for_boot_part; fi
nvme_init=if ${nvme_need_init}; then setenv nvme_need_init false; nvme scan; fi
preboot=usb start
pxefile_addr_r=0x40300000
ramdisk_addr_r=0x44000000
scan_dev_for_boot=echo Scanning ${devtype} ${devnum}:${distro_bootpart}...; for prefix in ${boot_prefixes}; do run scan_dev_for_extlinux; run scan_dev_for_scripts; done;run scan_dev_for_efi;
scan_dev_for_boot_part=part list ${devtype} ${devnum} -bootable devplist; env exists devplist || setenv devplist 1; for distro_bootpart in ${devplist}; do if fstype ${devtype} ${devnum}:${distro_bootpart} bootfstype; then run scan_dev_for_boot; fi; done; setenv devplist
scan_dev_for_efi=setenv efi_fdtfile ${fdtfile}; for prefix in ${efi_dtb_prefixes}; do if test -e ${devtype} ${devnum}:${distro_bootpart} ${prefix}${efi_fdtfile}; then run load_efi_dtb; fi;done;if test -e ${devtype} ${devnum}:${distro_bootpart} efi/boot/bootaa64.efi; then echo Found EFI removable media binary efi/boot/bootaa64.efi; run boot_efi_binary; echo EFI LOAD FAILED: continuing...; fi; setenv efi_fdtfile
scan_dev_for_extlinux=if test -e ${devtype} ${devnum}:${distro_bootpart} ${prefix}${boot_syslinux_conf}; then echo Found ${prefix}${boot_syslinux_conf}; run boot_extlinux; echo SCRIPT FAILED: continuing...; fi
scan_dev_for_scripts=for script in ${boot_scripts}; do if test -e ${devtype} ${devnum}:${distro_bootpart} ${prefix}${script}; then echo Found U-Boot script ${prefix}${script}; run boot_a_script; echo SCRIPT FAILED: continuing...; fi; done
scriptaddr=0x40200000
scsi_boot=run scsi_init; if scsi dev ${devnum}; then devtype=scsi; run scan_dev_for_boot_part; fi
scsi_init=if ${scsi_need_init}; then scsi_need_init=false; scsi scan; fi
stderr=pl011@9000000
stdin=pl011@9000000
stdout=pl011@9000000
usb_boot=usb start; if usb dev ${devnum}; then devtype=usb; run scan_dev_for_boot_part; fi
vendor=emulation
virtio_boot=run boot_pci_enum; run virtio_init; if virtio dev ${devnum}; then devtype=virtio; run scan_dev_for_boot_part; fi
virtio_init=if ${virtio_need_init}; then virtio_need_init=false; virtio scan; fi
```

首先把后续步骤里编译且裁减好的镜像转换成 uboot 可以识别的格式，再启动：
```sh
sudo apt install u-boot-tools
mkimage -A arm64 -C none -T kernel -a 0x40080000 -e 0x40080000 -n qemu-virt-hello -d target/aarch64-unknown-none/release/kernel.bin uImage
qemu-system-aarch64 -curses -machine virt -cpu cortex-a57 -bios /usr/lib/u-boot/qemu_arm64/u-boot.bin -serial stdio -device loader,file=uImage,addr=0x40400000 -s -S
```

不过由上可知 kernel 的 entry point 在 0x40400000, 与 qemu 默认的入口点设置不同，且我没有成功用 uboot 启动过，因此不使用 uboot 。