# Hello world

使用串口打印功能打印出 "hello world"

```sh
cargo build --release
rust-objcopy --strip-all target/aarch64-unknown-none/release/hello_world -O binary target/aarch64-unknown-none/release/hello_world.bin
# 可以查看文件头
rust-readobj -h target/aarch64-unknown-none/release/hello_world.bin
# 或者反汇编
rust-objdump -S target/aarch64-unknown-none/release/hello_world.bin

# 启动它
qemu-system-aarch64 -machine virt -cpu cortex-a57 -smp 1 -m 2G -kernel target/aarch64-unknown-none/release/hello_world.bin -serial file:/home/zhou/tmp -nographic -s -S

gdb -ex 'file target/aarch64-unknown-none/release/hello-world' \
    -ex 'set arch aarch64' \
    -ex 'target remote localhost:1234'
```

## 关于 UART 串口通信

CPU 在正常使用监视器等外设之前，只能通过串口收发信息来与用户交互。