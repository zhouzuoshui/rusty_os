// TODO: console 可以选择不同的底层实现，这里是 qemu, 也可以换成其他，但是接口必须一致
// 应当采用单例模式，且有锁

use core::convert::Infallible;
use ufmt::uWrite;
use crate::driver::serial_port::serial_qemu;

pub struct Console;

impl Console {
    pub fn init_console() {
        serial_qemu::init_serial();
    }

    pub fn putc(ch : u8) {
        serial_qemu::putc(ch);
    }

    pub fn getc() -> u8 {
        serial_qemu::getc()
    }
}

impl uWrite for Console {
    type Error = Infallible;

    // 实现了 write_str, traits 会自动创建 write_fmt 供 ufmt 的 write 宏使用
    fn write_str(&mut self, s: &str) -> Result<(), Infallible> {
        for c in s.chars() {
            Console::putc(c as u8);
        }

        Ok(())
    }
}

impl Console {
    // 应当实现一个读取输入的功能，但是目前没有堆，暂时无法实现
}

// 它应该是全局唯一的
pub fn get_console() -> Console {
    return Console{}
}

