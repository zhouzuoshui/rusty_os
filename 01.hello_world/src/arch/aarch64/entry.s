    .section .text.entry
    .global _start
    .type	_start, function
_start:
    adrp x0, boot_stack
    adrp x1, boot_stack_top
.L_bss_init:
    cmp x0, x1
    b.eq .L_go_to_rust_main
    # 快速清零
    # 把 xzr 和xzr 的值也就是0放入到 [x0] 和 [x0], #8, 因为这是个64位的寄存器，因此两个就是128位，即16字节，因此递增的幅度是16
    stp xzr, xzr, [x0], #16
    # 比这样快
    # mov [x0], #0x00000000
    # add x0, x0, #8
    b .L_bss_init
.L_go_to_rust_main:
    # 设置 sp 寄存器指向我们开辟的栈顶
    adrp x0, boot_stack_top
    add x0, x0, #:lo12:boot_stack_top
    mov sp, x0
    # 进入 rust
    bl rust_main
    .size	_start, . - _start


    .section .bss.stack
    .global boot_stack
boot_stack:
    .space 4096 * 16
    .global boot_stack_top
boot_stack_top:
