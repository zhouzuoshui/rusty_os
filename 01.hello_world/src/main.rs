#![no_std]
#![no_main]

mod rusty_panic_handler;
mod tools;
mod arch;
mod driver;
use ufmt::uwrite;


#[no_mangle]
pub unsafe fn rust_main() {
    tools::console::Console::init_console();
    uwrite!(tools::console::get_console(), "Hello world, in {}\n", 2022).expect("failed");

    loop {}
}
