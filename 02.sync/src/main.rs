#![no_std]
#![no_main]

mod rusty_panic_handler;
mod tools;
mod arch;
mod driver;
use ufmt::uwrite;


#[no_mangle]
pub unsafe fn rust_main() {
    tools::console::Console::init_console();
    let year : u16 = 2022;
    uwrite!(tools::console::get_console(), "Hello world, in {}\n", year).expect("failed");

    let mut mutex = crate::arch::aarch64::mutex::Mutex::new();
    uwrite!(tools::console::get_console(), "Mutex unlocked: {}\n", mutex.get_status()).expect("failed!");
    mutex.try_to_lock_mutex();
    uwrite!(tools::console::get_console(), "Mutex locked: {}\n", mutex.get_status()).expect("failed!");
    mutex.unlock_mutex();
    uwrite!(tools::console::get_console(), "Mutex unlocked: {}\n", mutex.get_status()).expect("failed!");

    loop {}
}
