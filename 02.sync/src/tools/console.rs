// console 可以选择不同的底层实现，这里是 qemu, 也可以换成其他，但是接口必须一致

use core::{convert::Infallible};
use ufmt::uWrite;
use crate::driver::serial_port::serial_qemu;

static mut UNI_CONSOLE : Option<Console> = None;

static mut CONSOLE_MTX : crate::arch::aarch64::mutex::Mutex = crate::arch::aarch64::mutex::Mutex {
    _status : 0,
};

pub struct Console {
}

impl Console {
    pub fn new() -> &'static mut Console {
        unsafe {
            CONSOLE_MTX.lock_mutex();
            match UNI_CONSOLE {
                Some(ref mut obj) => {
                    CONSOLE_MTX.unlock_mutex();
                    obj
                }
                None => {
                    UNI_CONSOLE = Some(Console{});
                    Console::init_console();
                    CONSOLE_MTX.unlock_mutex();
                    Console::new()
                }
            }
        }
    }

    pub fn init_console() {
        serial_qemu::init_serial();
    }

    pub fn putc(ch : u8) {
        serial_qemu::putc(ch);
    }

    pub fn getc() -> u8 {
        serial_qemu::getc()
    }
}

impl uWrite for Console {
    type Error = Infallible;

    // with `write_str` func defined, traits automatically implements func `write_fmt`
    // as a subroutine for ufmt's macro `write`
    fn write_str(&mut self, s: &str) -> Result<(), Infallible> {
        for c in s.chars() {
            Console::putc(c as u8);
        }

        Ok(())
    }
}

pub fn get_console() -> &'static mut Console {
    return Console::new()
}

