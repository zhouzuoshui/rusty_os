// qemu serial port

pub fn init_serial() {
    const PL011_UART0_BASE   : u64 =      0x9000000;
    const UART_RSR_ECR        : u64 =      0x04;
    const UART_CR              : u64 =      0x30;
    const UART_LCR_H          : u64 =      0x2c;

    const BASE_RSR_ECR_ADDR : u64 = PL011_UART0_BASE + UART_RSR_ECR;
    const BASE_CR_ADDR       : u64 = PL011_UART0_BASE + UART_CR;
    const BASE_LCR_H_ADDR   : u64 = PL011_UART0_BASE + UART_LCR_H;
    const UART_LCRH_WLEN_8  : u8 = 3 << 5;
    const ENABLE_UART_RX_TX : u64 = 0x0301;

    unsafe {
        core::ptr::write_volatile(BASE_RSR_ECR_ADDR as *mut u8, 0 as u8);
        core::ptr::write_volatile(BASE_CR_ADDR as *mut u8, 0 as u8);
        core::ptr::write_volatile(BASE_LCR_H_ADDR as *mut u8, UART_LCRH_WLEN_8 as u8);
        core::ptr::write_volatile(BASE_CR_ADDR as *mut u32, ENABLE_UART_RX_TX as u32);
    }
}

const PL011_UART0_BASE   : u64 =      0x9000000;
const UART_DR              : u64 =      0x00;

pub fn putc(ch : u8) {
    // let addr : u64 = (PL011_UART0_BASE + UART_DR) as u64;
    unsafe {
        core::ptr::write_volatile((PL011_UART0_BASE + UART_DR) as *mut u8, ch as u8);
    }
    // core::fmt::Result::Ok(())
}


const UART_FR              : u64 =      0x18;

pub fn getc() -> u8{
    let mut ch : u8 = 26 as u8; // eof
    unsafe {
        ch = core::ptr::read_volatile((PL011_UART0_BASE + UART_FR) as *mut u8);
    }
    ch
}