const MUTEX_LOCKED : u8 = 1;
const MUTEX_UNLOCKED : u8 = 0;

// TODO: 不应该设为 pub
pub struct Mutex {
    pub _status : u8
}

impl Mutex {
    pub fn new() -> Mutex {
        Mutex {
            _status : MUTEX_UNLOCKED
        }
    }

    pub fn get_status(&self) -> u8 {
        self._status
    }

    pub fn lock_mutex(&mut self) {
        let addr = core::ptr::addr_of!(self._status);

        unsafe {
            core::arch::asm!(
                "mov x0, {0}",
                "mov w1, {1:w}",
                "ldxrb w2, [x0]",
            "2:",
                "cmp w1, w2",              // test if mutex already locked
                "beq 3f",                   // if locked, wait until released
                "stxrb w3, w1, [x0]",     // not locked, attemp to lock it
                "cmp w3, 1",               // check if store exclusive failed
                "bne 2b",                   // failed, retry from 2
                "dmb 0b1111",              // no random execution, required before accessing protected resource
                "b 4f",                     // OK, jump out of the loop; in Armv6 should be `bx lr`, but no similar alternative found in Armv8
            "3:",
                "b 2b",                     // loop, waiting to get lock, to be otimizd, just rturn fals or st a tim thrs
            "4:",
                in(reg) addr,
                in(reg) MUTEX_LOCKED,
            );
        }
    }

    pub fn try_to_lock_mutex(&mut self) -> bool {
        let addr = core::ptr::addr_of!(self._status);
        let mut ret : i32;

        unsafe {
            core::arch::asm!(
                "mov x0, {0}",
                "mov w1, #1",
                "ldxrb w2, [x0]",
            "2:",
                "cmp w1, w2",              // test if mutex already locked
                "beq 3f",                   // if locked, wait until released
                "stxrb w3, w1, [x0]",     // not locked, attemp to lock it
                "cmp w3, 1",               // check if store exclusive failed
                "bne 2b",                   // failed, retry from 2
                "dmb 0b1111",              // no random execution, required before accessing protected resource
                "mov {1:w}, #1",          // set ret value
                "b 4f",                     // OK, jump out of the loop; in Armv6 should be `bx lr`, but no similar alternative found in Armv8
            "3:",
                // "b 2b",                  // loop, waiting to get lock, to be optimized, just return false or set a time thres
                "mov {1:w}, #0",
            "4:",
                in(reg) addr,
                out(reg) ret,
            );
        }
        ret != 0
    }

    pub fn unlock_mutex(&mut self) {
        let addr = core::ptr::addr_of!(self._status);

        unsafe {
            core::arch::asm!(
                "mov x0, {0}",
                "mov x1, {1:x}",
                "dmb 0b1111",                // required before releasing protected resource
                "str x1, [x0]",              // unlock mutex
                in(reg) addr,
                in(reg) MUTEX_UNLOCKED,
            );
        }
    }
}