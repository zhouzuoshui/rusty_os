# Sync

之前的打印功能需要调用终端，这个终端并不支持多线程下的操作。为此，需要实现同步机制，这里只实现最简单的互斥锁。

```sh
cargo build --release
rust-objcopy --strip-all target/aarch64-unknown-none/release/sync \
    -O binary target/aarch64-unknown-none/release/sync.bin
# 可以查看文件头
rust-readobj -h target/aarch64-unknown-none/release/sync.bin
# 或者反汇编
rust-objdump -S target/aarch64-unknown-none/release/sync.bin

# 启动它
qemu-system-aarch64 -machine virt -cpu cortex-a57 -smp 1 -m 2G \
    -kernel target/aarch64-unknown-none/release/sync.bin -serial file:/home/zhou/tmp -nographic -s -S

gdb -tui -ex 'file target/aarch64-unknown-none/release/sync' \
    -ex 'set arch aarch64' \
    -ex 'target remote localhost:1234'

# 在 gdb 中，开启显示汇编指令
(gdb) layout asm
```

